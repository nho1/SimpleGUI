(function(){
    // Functions
    function buildQuiz(currentSlide){
      // variable to store the HTML output
      const output = [];

      const answers = [];

      if(currentSlide < 10){

        for(letter in myQuestions[currentSlide].answers){
          answers.push(
            `<label>
              <input type="radio" name="question${currentSlide}" value="${letter}">
              ${letter} :
              ${myQuestions[currentSlide].answers[letter]}
              </label>`
            );
          }
        }
      // send question and answer to output
      output.push(
        ` <div class="question" style="text-align:center"> ${myQuestions[currentSlide].question} </div>
          <div class="answers" style="padding-left: 525px"> ${answers.join("<br>")} </div>`
      );

        quizContainer.innerHTML = output.join('');

        imageShown.src = imageSrcList[currentSlide];

        if(currentSlide === 0){
          previousButton.style.display = 'none';
        }
        else{
          previousButton.style.display = 'inline-block';
        }
        if(currentSlide === 9 && alreadyClicked === 0){
          nextButton.style.display = 'none';
          submitButton.style.display = 'inline-block';
          previousButton.style.display = 'inline-block';
          console.log("Submit button has not been clicked")
        }else if (currentSlide === 9 && alreadyClicked === 1){
          console.log("We got to the correct inline block")
          submitButton.style.display = 'none';
          nextButton.style.display = 'none';
          previousButton.style.display = 'none';
          resetButton.style.display = 'inline-block';
        }else{
          nextButton.style.display = 'inline-block';
          submitButton.style.display = 'none';
          resetButton.style.display = 'none';
        }
        if(currentSlide >= 10){
          nextButton.style.display = 'none';
          submitButton.style.display = 'none';
          previousButton.style.display = 'none';
          resetButton.style.display = 'inline-block';
        }

      }


    function saveAnswer(){
        const answerContainer = quizContainer.lastElementChild;

        const selector = `input[name=question${currentSlide}]:checked`;
        const userAnswer = (answerContainer.querySelector(selector) || {}).value;

        //Add a number to the specified reference in the array (each corresponding to a language)
        if(userAnswer === myQuestions[currentSlide].correctAnswer[0]){
          answerCounters[1]++;
        }else if (userAnswer === myQuestions[currentSlide].correctAnswer[1]){
          answerCounters[2]++;
        }else if (userAnswer === myQuestions[currentSlide].correctAnswer[2]){
          answerCounters[3]++;
        }else if (userAnswer === myQuestions[currentSlide].correctAnswer[3]){
          answerCounters[4]++;
        }else{
         console.log("Some user skipped this question");
        }
    }

    function showResults(){

      //Find the reference/place of the max value of the array
      let i = answerCounters.indexOf(Math.max(...answerCounters));

      //Print out corresponding language
      if(i === 1){
        resultsContainer.innerHTML = `Your coding language is Java!  You are reliable and useful in a variety of contexts.
          You enjoy structure and ensuring everything is organized (you sure do like labeling things).`;
        imageShown.src = "https://cdn.vox-cdn.com/thumbor/VoXJ8IaxCj5_U-366JhtUHLkdQ0=/0x0:640x427/1400x1050/filters:focal(0x0:640x427):format(jpeg)/cdn.vox-cdn.com/assets/1087137/java_logo_640.jpg";
      }else if (i === 2){
        resultsContainer.innerHTML = `Your coding language is C++!  You are extremely logical and don't mind spending lots of time
        working on projects/work.  You do things in a very analytical matter and perhaps prefer the being inside.`;
        imageShown.src = "https://cdn.dribbble.com/users/46200/screenshots/799814/cpp-logo-dribbble.png";
      }else if (i === 3){
        resultsContainer.innerHTML = `Your coding language is JavaScript!  You like doing things/working for a specific tangible purpose.  While you
        can work alone if necessary, you prefer to share the work with a team to meet a certain goal.`;
        imageShown.src = "https://thumbs.dreamstime.com/b/javascript-logo-javascript-logo-white-background-vector-format-available-136765881.jpg";
      }else if (i === 4){
        resultsContainer.innerHTML = `Your coding language is Python!  You enjoy your freetime/time to be active, so you like to get in,
        get your work done, and get out to enjoy your day.  You love finding the quickest way to do things and you focus on the big picture
        instead of minute details.`;
        imageShown.src = "https://www.nicepng.com/png/detail/893-8937286_python-logo.png";
      }else{
        //Catches all edge cases (in which case you really do deserve Lisp)
        resultsContainer.innerHTML = `Your coding language is Lisp...which means you might have skipped a question!`;
        imageShown.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Lisp_logo.svg/1200px-Lisp_logo.svg.png";
      }

    }

    function showNextSlide() {
      currentProgress += 10;
      progressBar.style.width = width + "%";
      progressBar.innerHTML = currentProgress + "%";
      currentSlide = currentSlide + 1;
      buildQuiz(currentSlide);
    }

    function showNextSlideOnSubmit() {
      console.log("Submit button has been clicked");
      currentProgress += 10;
      progressBar.style.width = width + "%";
      progressBar.innerHTML = currentProgress + "%";
      currentSlide = currentSlide + 1;
      buildQuiz(currentSlide);
    }

    function showPreviousSlide() {
      currentProgress -= 10;
      progressBar.innerHTML = currentProgress + "%";
      currentSlide = currentSlide - 1;
      buildQuiz(currentSlide);
      //Need to add this to remove text if previous question is put in
      resultsContainer.innerHTML = ``;
    }

    function submitClicked() {
      alreadyClicked = 1;
    }


    //Moves the progress bar to the correct spot
    function moveProgressBar() {
      if (i == 0) {
        i = 1;
        var id = setInterval(frame, 10);
          function frame() {
            if (width > currentProgress) {
              width--;
              progressBar.style.width = width + "%";
            } else if (width < currentProgress){
              width++;
              progressBar.style.width = width + "%";
            }else{
              clearInterval(id);
              i = 0;
            }
          }
        }
    }

     // Variables
    const resultsContainer = document.getElementById('results');
    const submitButton = document.getElementById('submit');
    const quizContainer = document.getElementById('quiz');
    const resetButton = document.getElementById('reset');
    var progressBar = document.getElementById("myBar");
    var imageShown = document.getElementById("currentImg");
    var width = 0;
    var currentProgress = 0;
    var i = 0;
    var alreadyClicked = 0;

    const imageSrcList = [
      "https://i.pinimg.com/originals/67/70/46/677046f3ecce923bd242e95a29b9c74a.jpg",
      "https://ussunsolar.com/wp-content/uploads/2018/04/solar-for-all-seasons.jpg",
      "https://uknow.uky.edu/sites/default/files/styles/uknow_story_image/public/GettyImages-1160947136%20%281%29.jpg",
      "https://www.english-learn-online.com/wp-content/uploads/free-time-activities-696x373.jpg",
      "https://qph.fs.quoracdn.net/main-qimg-bdc1a0f1f445e182acb3f0337ea5e637",
      "https://cdn.images.express.co.uk/img/dynamic/36/590x/lord-of-the-rings-trilogy-star-wars-pirates-1160711.webp?r=1564682577013",
      "https://www.musicplus.in/wp-content/uploads/2020/09/Music_Genre_Feature.jpg",
      "https://i.pinimg.com/originals/05/19/e1/0519e12b4ca83b29ae7c88f33e33876b.jpg",
      "https://kubrick.htvapps.com/htv-prod-media.s3.amazonaws.com/images/sports-1584678012.jpg?crop=1.00xw:1.00xh;0,0&resize=1200:*",
      "http://www.smh.com.au/content/dam/images/h/1/f/z/x/v/image.imgtype.articleLeadwide.620x349.png/1562315678260.png"
    ]

    const myQuestions = [
      {
        question: "What is your favorite color in this group?",
        answers: {
          a: "Red",
          b: "Blue",
          c: "Green",
          d: "Yellow"
        },
        correctAnswer: ["a", "b", "d", "c"]
      },
      {
        question: "What is your favorite season?",
        answers: {
          a: "Summer",
          b: "Spring",
          c: "Winter",
          d: "Fall"
        },
        correctAnswer: ["b", "c", "d", "a"]
      },
      {
        question: "Which would you most want to vacation to?",
        answers: {
          a: "Hawaii",
          b: "United Kingdom",
          c: "Japan",
          d: "Australia"
        },
        correctAnswer: ["a", "b", "d", "c"]
      },
      {
        question: "Which would you rather do in your freetime?",
        answers: {
          a: "Watch a movie",
          b: "Read a book",
          c: "Go on a walk/hike",
          d: "Sleep"
        },
        correctAnswer: ["a", "b", "d", "c"]
      },
      {
        question: "Which would you eat right now?",
        answers: {
          a: "Hamburger",
          b: "Burrito",
          c: "Pasta",
          d: "Sushi"
        },
        correctAnswer: ["b", "c", "d", "a"]
      },
      {
        question: "Which series would you rewatch?",
        answers: {
          a: "Star Wars",
          b: "Lord of the Rings",
          c: "Harry Potter",
          d: "Pirates of the Carribean"
        },
        correctAnswer: ["a", "b", "d", "c"]
      },
      {
        question: "What genre of music do you enjoy the most?",
        answers: {
          a: "Pop",
          b: "Hip-hop/Rnb",
          c: "Rock/Alternative",
          d: "Classical"
        },
        correctAnswer: ["a", "d", "c", "b"]
      },
      {
        question: "What is your favorite day of the week?",
        answers: {
          a: "Friday",
          b: "Saturday",
          c: "Sunday",
          d: "Monday"
        },
        correctAnswer: ["a", "d", "c", "b"]
      },
      {
        question: "What is your favorite sport to play?",
        answers: {
          a: "Basketball",
          b: "Football",
          c: "Baseball/Softball",
          d: "ESports"
        },
        correctAnswer: ["a", "d", "c", "b"]
      },
      {
        question: "Which is your favorite part of nature?",
        answers: {
          a: "Mountains",
          b: "Cliffs/Canyons",
          c: "Rivers/Lakes",
          d: "Forests/Rainforests"
        },
        correctAnswer: ["a", "d", "b", "c"]
      },
      {
              question: "",
              answers: {
                a: "",
                b: "",
                c: "",
                d: ""
              },
              correctAnswer: ""
      }
    ];

    // Create the quiz
    const previousButton = document.getElementById("previous");
    const nextButton = document.getElementById("next");
    let answerCounters = [0,0,0,0,0];
    let currentSlide = 0;
    buildQuiz(currentSlide);


    // Event listeners
    submitButton.addEventListener('click', submitClicked);
    submitButton.addEventListener('click', saveAnswer);
    submitButton.addEventListener('click', showNextSlideOnSubmit);
    submitButton.addEventListener('click', showResults);
    submitButton.addEventListener('click', moveProgressBar);
    previousButton.addEventListener("click", showPreviousSlide);
    nextButton.addEventListener("click", saveAnswer);
    nextButton.addEventListener("click", showNextSlide);
    previousButton.addEventListener("click", moveProgressBar);
    nextButton.addEventListener("click", moveProgressBar);
  })();
